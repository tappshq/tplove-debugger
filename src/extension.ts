'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

let taskProvider: vscode.Disposable | undefined;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "tplove-debugger" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.sayHello', () => {
        // The code you place here will be executed every time your command is executed

        // Display a message box to the user
        vscode.window.showInformationMessage('Hello World!');
    });

    context.subscriptions.push(disposable);

    let workspaceRoot = vscode.workspace.rootPath;
    if (!workspaceRoot) {
        return;
    }
    let rakePromise: Thenable<vscode.Task[]> | undefined = undefined;

    let fileWatcher = vscode.workspace.createFileSystemWatcher("GameConfig.json");
    fileWatcher.onDidChange(() => rakePromise = undefined);
    fileWatcher.onDidCreate(() => rakePromise = undefined);
    fileWatcher.onDidDelete(() => rakePromise = undefined);

    taskProvider = vscode.workspace.registerTaskProvider('tplove', {
        provideTasks: () => {
            if (!rakePromise) {
                rakePromise = getRakeTasks();
            }
            return rakePromise;
        },
        resolveTask(_task: vscode.Task): vscode.Task | undefined {
            return undefined;
        }
    });
}

// this method is called when your extension is deactivated
export function deactivate() {
}

interface TPLoveTaskDefinition extends vscode.TaskDefinition {

}

async function getRakeTasks(): Promise<vscode.Task[]> {
    let workspaceRoot = vscode.workspace.rootPath;
    let emptyTasks: vscode.Task[] = [];
    if (!workspaceRoot) {
        return emptyTasks;
    }

    return vscode.workspace.findFiles("**/GameConfig.json").then(
        (gameConfigs) => {
            let gameTasks: vscode.Task[] = [];
            let tploveConfig = vscode.workspace.getConfiguration("tplove");

            let tploveCore = tploveConfig.get<string>("tploveCorePath");

            for (let gameConfig of gameConfigs) {
                let gamePath = gameConfig.fsPath.replace("GameConfig.json", "");

                let kind: TPLoveTaskDefinition = {
                    type: "tplove",
                    task: gamePath
                };

                let options: vscode.ProcessExecutionOptions = {
                    cwd: workspaceRoot
                };
                let process = new vscode.ProcessExecution(
                    tploveConfig.get("tplovePath"),
                    [
                        "--tplove",
                        tploveCore,
                        gamePath
                    ],
                    options
                )
                gameTasks.push(new vscode.Task(kind, gamePath, "tplove", process));
            }
            return gameTasks;
        },
        () => emptyTasks
    );
}
